import React from 'react';
import styled from 'styled-components';
import Logo from '../../components/logo'
import Input from '../../components/input'

import axios from 'axios';
    
const Login: React.FC =  () =>{


    return(
        <Wrapper>
            <div className="ContentLogin">
                <Logo/>

                <form action="">
                    <h2>
                        Bem vindo(a) de volta!
                    </h2>
                    <p>
                        Acesse sua conta:
                    </p>

                    <Input placeholder="Usuário" />

                    <Input placeholder="Senha" type="password"/>
                    <div className="formGroup">
                        <label htmlFor="saveLogin">
                            <Input type="checkbox" name="saveLogin" id="saveLogin"/>
                            <small>Salvar login</small>
                        </label>
                        <a> Esqueci a senha</a>
                    </div>

                    <button>Entrar</button>

                    <p className='loginText'> <small> Ainda não tem o login?</small> <a href="#" target="_blank" rel="noopener noreferrer"> Cadastre-se</a></p>
                </form>
                
            </div>
        </Wrapper>
    )
}



const Wrapper = styled.section`
    @keyframes change {
        0% {width:100%;}
        100% {width:50%;}
    }

    @keyframes showForm {
        0% {
            visibility:hidden;
            height:0px;
            overflow:hidden;
        }
        50% {
            visibility:hidden;
        }
        75% {
            visibility:hidden;
        }
        95% {
            height:20px;
            visibility:visible;
        }
        100% {
            overflow:hidden;
            visibility:visible;
            height:initial;
        }
    }

    background:#000;
   

    display:flex;
    justify-content: start;
    align-items:center;
    width:100vw;
    height: 100vh;
    overflow:hidden;
    position: relative;
    z-index:333;
    :before{
        content: ' ';
        width:40vw;
        height:100vh;
        display:block;
        background-image:url('/Ultimato.jpg');
        background-position-x: 10%;
        background-position-y: 0;
        position:absolute;
        right:0px;
        background-size: auto 100%;
        background-repeat: no-repeat;
        z-index: 333;
        opacity:0;

        animation-duration: 2.6s;
        animation-name: opacity;
        animation-iteration-count: 1;

        opacity:1;
    }
    :after{
        content: ' ';
        width:50%;
        height:100vh;
        background: linear-gradient(90deg,rgba(0,0,0,1) 40%,rgba(0,0,0,0.5) 100%);
        position:absolute;
        right:0px;
        z-index:333;

    }
    .ContentLogin{
        display:flex;
        justify-content: center;
        animation-name: change;
        align-items:center;
        background:#000;
        height:100%;
        width:50%;
        flex-direction:column;
        animation-duration: 1.3s;
        animation-timing-function: cubic-bezier(0.67, 0.02, 0.98,-0.15);
        animation-iteration-count: 1;
        position: relative;
        z-index:444;
    }

    form{
        width: 383px;
        height:1px;
        display:flex;
        visibility:hidden;
        animation-duration: 2.6s;
        animation-name: showForm;
        visibility:visible;
        animation-timing-function: ease-in-out;
        animation-iteration-count: 1;
        height:initial;
        flex-direction:column;
    }

    h2{
        color:${props => props.theme.colors.primary};
        font-family: 'Axiforma';
        font-weight: 600;
        font-size:30px;
        margin-top:55px;
    }

    form p{
        color:${props => props.theme.colors.text};
        font-family: 'Axiforma';
        font-weight: 100;
        font-size:20px;
        margin-bottom:25px;
    }

    input[type="checkbox"]{
        display:inline-block;
        width:13px;
        height:14px;
        border: solid 2px ${props => props.theme.colors.primary};
        margin-bottom:0px;
    }
    form label{
        position:relative;
        z-index:2;
    }

    form label small{
       margin-left:10px;
       font-size:15px;
       color: ${props => props.theme.colors.text}
       font-family: 'Axiforma';
       font-weight: 400;
    }

    form label input[type="checkbox"]::after{
        content:" ";
        width:13px;
        height:14px;
        display:inline-block;
        background:#fff;
        border: solid 2px ${props => props.theme.colors.primary};
        position:absolute;
        border-radius:4px;
        top:0px;
        z-index:1;
        transition: all 0.2s ease-out
    }

    form label input[type="checkbox"]:checked::after{
        background:${props => props.theme.colors.primary};
    }
    
    form .formGroup a{
       margin-left:10px;
       font-size:15px;
       font-family: 'Axiforma';
       font-weight: 400;
       text-decoration: none;
       padding-bottom:2px;
       border-bottom:solid 1px ${props => props.theme.colors.primary};
    }
    .formGroup{
        width:100%;
        justify-content: space-between;
        display: flex;
        align-items: center;
    }
    button{
        display:block;
        margin:23px 0;
        width:100%;
        border-radius:5em;
        background:${props => props.theme.colors.primary};
        padding-top:22px ;
        padding-bottom:13px ;
        border:none;
        font-family: 'Axiforma';
       font-weight: 400;
       font-size:28px;
       color:#fff
    }

    .loginText{
        text-align:center;
        font-family: 'Axiforma';
       font-weight: 400;
       font-size:16px;
    }

    .loginText a{
        color:${props => props.theme.colors.primary};
        text-decoration:none;
    }
    @keyframes opacity {
        0% {opacity:0;}
        50% {opacity:0;}

        100%{opacity:1;}
    }



`;

// Login.getInitialProps = async () =>{
//     const response = await axios.get('http://localhost:3000/api/images');
//     console.log(response.data)
//     return { dados:response}
// }
export default Login