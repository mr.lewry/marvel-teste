import React,{ Fragment } from 'react';
import styled from 'styled-components';
import DefaultCarousel from '../../components/itemsCarousel';
import Nav from '../../components/Nav';
const dashboardWrapper: React.FC =  () =>{

    return(
        <Fragment>

        <Nav />
        <DefaultCarousel/>
        </Fragment>
    )
}

export default dashboardWrapper