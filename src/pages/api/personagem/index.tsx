import { NextApiRequest, NextApiResponse } from 'next';
import axios from 'axios';




const handler = (_req , res ) => {
    try{
        res.status(200).json([
            {   id: 2, 
                name: 'Homen-Aranha', 
                excerpt:'Após ser mordido por uma aranha radioativa, Peter Parker se torna o amigo da vizinhança, o Homem-Aranha.', 
                showIn: [
                    'vingadores - Era de Ultron',
                    'Capitão América - Guerra Civil',
                    'Vingadores - Guerra Infinita',
                ]
            },
            {   id: 1, 
                name: 'Wanda Vision', 
                excerpt:'Após ser mordido por uma aranha radioativa, Peter Parker se torna o amigo da vizinhança, o Homem-Aranha.', 
                showIn: [
                    'vingadores - Era de Ultron',
                    'Capitão América - Guerra Civil',
                    'Vingadores - Guerra Infinita',
                ]
            },
            // { id: 2, name: 'ultimato', src:'C:/\Users/\leeon/\Documents/\Imagens-Marvel-Teste/\Ultimato.jpg'}
            // { id: 2, name: 'ultimato', src:'C:/\Users/\leeon/\Documents/\Imagens-Marvel-Teste/\Ultimato.jpg'}
            // { id: 2, name: 'ultimato', src:'C:/\Users/\leeon/\Documents/\Imagens-Marvel-Teste/\Ultimato.jpg'}
            // { id: 2, name: 'ultimato', src:'C:/\Users/\leeon/\Documents/\Imagens-Marvel-Teste/\Ultimato.jpg'}
            // { id: 2, name: 'ultimato', src:'C:/\Users/\leeon/\Documents/\Imagens-Marvel-Teste/\Ultimato.jpg'}
        ]);
    } catch (err){
        res.status(500).json({statusCode: 500, message: err.message});
    }
};

export default handler;