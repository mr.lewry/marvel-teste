import React from 'react'
import Head from 'next/head'


import { Container } from '../styles/pages/Home'
import  Logo  from '../components/logo'

const Home: React.FC = () => {
  return (
    <Container>
      <Head>
        <title>Marvel</title>
      </Head>

      <Logo />
    </Container>
  )
}

export default Home
