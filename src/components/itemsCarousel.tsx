
import ItemsCarousel from 'react-items-carousel';
import Card from '../components/cards' 

import axios from 'axios';

// const MyItems = async () => {
//     try {
//         const resp = await axios.get('http://localhost:3000/api/personagem');
//         return resp.data;
//     } catch (err) {
//         // Handle Error Here
//         console.error(err);
//     }
// };

// const MyItems = () => {
    
//     // let items  = MyItems();
//     const [activeItemIndex, setActiveItemIndex] = useState(0);
//     const chevronWidth = 40;
//     const [repos] = useState(0);
    
//     // console.log('dados');
//     // console.log(dados);

//     axios.get('http://localhost:3000/api/personagem'){
//         .then(response =>({ repos: response.data }));
//         .catch(error => console.log(error));
//     };

//     // brendao =  async () => {
//     //     // const _res = await axios.get('http://localhost:3000/api/personagem');
//     //     // return { dados: _res.data}
//     //     try {
//     //         const resp = await axios.get('http://localhost:3000/api/personagem');
//     //         return resp.data;
//     //     } catch (err) {
//     //         // Handle Error Here
//     //         console.error(err);
//     //     }
//     // }

//     return (
//         <StyleCarrousel >
//             <div style={{ padding: `0 ${chevronWidth}px` }}>
//             <ItemsCarousel
//                 requestToChangeActive={setActiveItemIndex}
//                 activeItemIndex={activeItemIndex}
//                 numberOfCards={3}
//                 gutter={20}
//                 leftChevron={<button>{'<'}</button>}
//                 rightChevron={<button>{'>'}</button>}
//                 outsideChevron
//                 chevronWidth={chevronWidth}
//             >
//                 <Card>
//                     First card
//                 </Card>
//                 {console.log(repos)}

                
//                 {/* <Repos/> */}
//             </ItemsCarousel>
//             </div>
//         </StyleCarrousel>
//     );
// };






import React from 'react';
import range from 'lodash/range';
import styled from 'styled-components';
// import ItemsCarousel from '../../src/ItemsCarousel';

const noOfItems = 12;
const noOfCards = 3;
const autoPlayDelay = 2000;
const chevronWidth = 40;
const items = [];

const Wrapper = styled.div`
  padding: 0 ${chevronWidth}px;
  /* max-width: 1000px; */
  /* margin: 0 auto; */
`;

const StyleCarrousel = styled.div`
    background:#000;
    display:block;
    height:calc(100vh - 114px);
    padding-top:135px;
    

`;

const SlideItem = styled.div`
  height: 200px;
  background: #EEE;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 20px;
  font-weight: bold;
`;

// const carouselItems = range(noOfItems).map(index => (
//   <SlideItem key={index}>
//     {index+1}
//   </SlideItem>
// ));



// const MyItems = async () => {
//     try {
//         const resp = await axios.get('http://localhost:3000/api/personagem');
//         console.log(resp.data);
//         // items = resp.data;
//         // return resp.data;
//     } catch (err) {
//         // Handle Error Here
//         console.error(err);
//     }
// };
// MyItems();
const carouselItems = range(noOfItems).map(index => (
    <SlideItem key={index}>
      {index+1}
    </SlideItem>
  ));
// MyItems();

export default class AutoPlayCarousel extends React.Component {
    
  state = {
    activeItemIndex: 0,
    repos: []
  };

//   constructor(props) {
//     super(props);
//     this.state = {repos: []};
//   }
  componentDidMount() {
    this.interval = setInterval(this.tick, autoPlayDelay);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
    axios.get('http://localhost:3000/api/personagem')
        .then(response => this.setState({ repos: response.data }))
        .catch(error => console.log(error));
  }

  tick = () => this.setState(prevState => ({
    activeItemIndex: (prevState.activeItemIndex + 1) % (noOfItems-noOfCards + 1),
  }));

  onChange = value => this.setState({ activeItemIndex: value });
//   carouselItemss = 
  render() {
    return (
      <StyleCarrousel>
        <Wrapper>
            <ItemsCarousel
            gutter={12}
            numberOfCards={noOfCards}
            activeItemIndex={this.state.activeItemIndex}
            requestToChangeActive={this.onChange}
            rightChevron={<button>{'>'}</button>}
            leftChevron={<button>{'<'}</button>}
            chevronWidth={chevronWidth}
            outsideChevron
            children={carouselItems}
            />
            
            <ItemsCarousel/>
        </Wrapper>
      </StyleCarrousel>
    );
  }
}
