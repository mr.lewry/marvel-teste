import styled from 'styled-components';


const Logo = () =>{
    return(
    <ContentLogo className='logo'>
        <p>
            MARVEL
        </p>
    </ContentLogo>
    )
}

const ContentLogo = styled.div`
    font-family:Marvel;
    background:#ff0000;
    width: auto;
    line-height:95px;
    height:95px;
    max-width:237px;
    margin-top:0px;
    display:flex;
    align-items: center;
    justify-content: center;
    padding:5px;

    p{
        font-size:100px;
        line-height:100%;
        height:100%;
        color:#fff;
    }
`;

export default Logo