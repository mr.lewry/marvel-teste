import styled from 'styled-components';

const Input = styled.input`
    display:block;
    width:100%;
    max-width:383px;
    height:73px;
    /* width:20px; */
    background:#fff;
    border:none;
    border-radius:5em;
    margin-bottom:18px;
    padding:  25px;
    color: #d1d1d6;
    font-family:'Axiforma';
    font-weight:100;
    font-size:22px;
    ::placeholder {
        color: #d1d1d6;
        font-family:'Axiforma';
        font-weight:100;
    }
    :focus {
        box-shadow: 0 0 0 0;
        border: 0 none;
        outline: 0;
    } 
    
`;

export default Input;
