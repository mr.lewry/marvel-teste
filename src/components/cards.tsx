import styled from 'styled-components';
import axios from 'axios';

const getInfos = async () => {
    try {
        const resp = await axios.get('http://localhost:3000/api/personagem');
        console.log(resp.data);
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
};

const Card = (props) =>{


    // getInfos();
    return(
    <ContentCard className='logo'>
        <div>
            <h5>
                {props.name}
            </h5>
            <p>
                {props.excerpt}
            </p>
            <a href="http://">ver detalhes</a>
        </div>
    </ContentCard>
    )
}

// Card.getInitialProps = async ctx =>{
//     const response = await axios({
//         method:'get',
//         url: 'http://localhost:3000/api/personagem'
//     });

//     console.log(response)
// }

const ContentCard = styled.div`
    background: url('https://via.placeholder.com/150');
    width:289px;
    height: 439px;
    display:flex;
    background:purple;
    border-radius:30px;
    display:flex;
    justify-content: flex-end;
    align-items: flex-end;
    div{
        padding: 23px;
        border-radius:30px;
        width:100%;
        height:234px;
        background:linear-gradient( ${props => props.theme.colors.primary}, transparent);
        color:#fff;
        text-align: center;

        h5{
            font-family:'Axiforma';
            font-weight:bold;
            font-size:20px;
        }

        p{
            font-family: 'Axiforma';
            font-weight:100;
            font-size: 12px;
        }

        a{
            font-family: 'Axiforma';
            font-weight:100;
            font-size: 20px;
            color:#fff;
            text-decoration:none;
        }
    }
`;

export default Card;