import ActiveLink from './ActiveLink'
import styled from 'styled-components';
import Logo from './logo';
const Nav = () => (
  <NavBar>
      <Logo/>

    <ul className="nav">
      <li>
        <ActiveLink activeClassName="active" href="/home">
          <a className="nav-link">Personagens</a>
        </ActiveLink>
      </li>
      <li>
        <ActiveLink activeClassName="active" href="/Filmes">
          <a className="nav-link">Filmes</a>
        </ActiveLink>
      </li>
      <li>
        <ActiveLink activeClassName="active" href="/HQs" as="/dynamic-route">
          <a className="nav-link">HQs</a>
        </ActiveLink>
      </li>
      <li>
        <ActiveLink activeClassName="active" href="/login" as="/dynamic-route">
          <a className="nav-link">Sair</a>
        </ActiveLink>
      </li>
    </ul>
  </NavBar>
)

const NavBar = styled.nav`
    height:114px;
    background:#000;
    display:flex;
    align-items:center;
    justify-content:space-between;
    padding: 0 60px;
    box-shadow: 1px -1px 10px 4px  ${props => props.theme.colors.primary};
    position: relative;
    z-index: 55;
    .logo{
        max-width:122px;
        height:54px;
        margin-left:20px;
        padding:0px;
    }

    .logo p{
        font-size:50px;
        line-height:66px;
    }

    ul{
        display:flex;
        width:50%;
        justify-content:space-between;
        /* background:#f1f2f3f6 */
    }

    ul li{
        list-style:none;
    }

    ul li a{
        font-family:'Axiforma';
        font-weight:Bold;
        font-size: 30px;
        color: #707070;


    }
    .nav-link {
        text-decoration: none;
    }
    .active{
        color: #ffffff;
    }
    /* .active:after {
        content: ' (current page)';
    } */
`;


export default Nav