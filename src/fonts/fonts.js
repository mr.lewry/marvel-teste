import { createGlobalStyle } from 'styled-components';

// import MarvelRegular  from ./marvelFont/MarvelRegular.ttf;

export default createGlobalStyle`
    @font-face {
        font-family: 'Marvel';
        src: local('Marvel'), local('MarvelRegular'),
        url(./marvelFont/MarvelRegular.ttf) format('ttf'),
        font-weight: 300;
        font-style: normal;
    }
    @font-face {
        font-family: 'Axiforma';
        src: local('Axiforma'), local('AxiformaLight'),
        url(./Axiforma/KastelovAxiformaLight.otf) format('otf'),
        font-weight: 200;
    }
    @font-face {
        font-family: 'Axiforma';
        src: local('Axiforma'), local('AxiformaThin'),
        url(./Axiforma/KastelovAxiformaThin.otf) format('otf'),
        font-weight: 100;
        font-style: Thin;
    }

    @font-face {
        font-family: 'Axiforma';
        src: local('Axiforma'), local('AxiformaLight'),
        url(./Axiforma/KastelovAxiformaBold.otf) format('otf'),
        font-weight: 600;
        font-style: Bold;
    }
    @font-face {
        font-family: 'Axiforma';
        src: local('Axiforma'), local('AxiformaMedium'),
        url(./Axiforma/KastelovAxiformaMedium.otf) format('otf'),
        font-weight: 400;
        font-style: Medium;
    }
`;